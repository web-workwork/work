$( document ).ready(function() {

	var growth = $( "[name=growth]" );//рост
	var weight = $( "[name=weight]" );//вес
	var submit = $( "[name=submit]" );
	var result = 0;
	function valid(variable) {
		variable = variable.val();
		if(variable>0 && variable<1000) {
			return variable;
		}
		return false;
	}
	function getRes() {
		growthVal = growth.val();
		weightVal = weight.val();
		result = (weightVal/Math.pow(growthVal,2))*10000;
		result = result.toFixed(2);
		alert(result);
	}
    submit.click(function(e) {
    	e.preventDefault();
    	if(valid(growth)&&valid(weight)) {
    		getRes();
    	} else {
    		alert("Введите валидные данные");
    	}
	});
});